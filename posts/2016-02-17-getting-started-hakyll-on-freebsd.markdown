---
title: Getting started with hakyll on FreeBSD
---

Since I have recently switched my site over to be on FreeBSD 10.2 server I figured it would be a good time to write up how I got hakyll installed.
Setting it up on Debian turned out to be a bit of an ordeal the first few times, so I was very pleasantly surprised to find out how much easier it was to install on FreeBSD.

There is no hakyll package for FreeBSD so we will be installing cabal and installing via that, which is better anyway.  I am including tmux here because if you have built anything haskell related before you know it takes quite awhile.  So I recommend running the install job in a tmux session.

```
pkg install hs-cabal-install tmux
```

Next we start up cabal, note again I am starting a tmux session because this could take some time.
```
tmux
cabal update
Config file path source is default config file.
Config file /root/.cabal/config not found.
Writing default configuration to /root/.cabal/config
Downloading the latest package list from hackage.haskell.org
```

Now that cabal is all set, lets actually install hakyll.
```
cabal install hakyll
```

Then go take a break, this will be awhile on a 1Gb SSD vps it took - 1 hour and 30 minutes.

Once it finishes you need to add your `$HOME/.cabal/bin` to your path or run
`./.cabal/bin/hakyll-init your-site-name-here`
Since you will really only need to "init" your site once.

One more thing and you are all ready to build your hakyll site.

Next we need to enable UTF-8 on Freebsd otherwise hakyll gives an error like:
commitBuffer: invalid argument (invalid character)

When you try to build your site.

The Hakyll documentation states to try:
```
LANG=en_US.UTF-8 ./site build
```
But I still was receiving the same error. So...

To fix edit `/etc/login.conf`

```
vi /etc/login.conf
```
and at the bottom of the **default** block add 
```
:charset=UTF-8:\
:lang=en_US.UTF-8:
```
Then reboot the machine or run `cap_mkdb /etc/login.conf` and you should be all set.

Another note is that I ran all this as root, you will want to do that or run with sudo so that hakyll gets installed systemwide (if you are okay with that).
